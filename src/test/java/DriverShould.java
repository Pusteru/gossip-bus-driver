import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;


public class DriverShould {

    public static final int STOP_ID = 1;

    @Test
    public void driver_should_share_gossip_when_are_in_the_same_stop(){
        Drivers drivers = new Drivers();
        drivers.add(new Driver(1, new Gossips(new Gossip(1)), getStop()));
        drivers.add(new Driver(2,new Gossips(new Gossip(2)), getStop()));

        drivers.shareGossips();
        Gossips expectedGossip = new Gossips();
        expectedGossip.add(new Gossip(1));
        expectedGossip.add(new Gossip(2));

        assertThat(drivers.getGossipsFrom(1)).isEqualTo(expectedGossip);
    }

    @Test
    public void changeStop() {
        Driver driver = new Driver(1, new Gossips(new Gossip(1)), getStops());
        driver.nextStop();
        assertThat(driver.isInStop(new Stop(2))).isTrue();
    }

    @Test
    public void share_gossips_when_met_with_another_driver() {
        Driver driver = new Driver(1, new Gossips(new Gossip(1)), getStops());
        Driver driver2 = new Driver(2, new Gossips(new Gossip(2)), getStops2());

        assertThat(driver.sameStopAs(driver2)).isFalse();

        driver.nextStop();
        driver2.nextStop();

        assertThat(driver.sameStopAs(driver2)).isTrue();
    }

    @Test
    public void share_gossip_when_lopping() {
        Driver driver = new Driver(1, new Gossips(new Gossip(1)), getStops());
        Driver driver2 = new Driver(2, new Gossips(new Gossip(2)), getStops3());

        assertThat(driver.sameStopAs(driver2)).isFalse();

        driver.nextStop();
        driver.nextStop();

        driver2.nextStop();
        driver2.nextStop();

        assertThat(driver.sameStopAs(driver2)).isTrue();
    }



    private Stops getStops3() {
        Stops stops = new Stops();
        stops.add(new Stop(2));
        stops.add(new Stop(3));
        stops.add(new Stop(1));
        return stops;
    }

    private Stops getStops2() {
        Stops stops = new Stops();
        stops.add(new Stop(2));
        stops.add(new Stop(2));
        return stops;
    }

    private Stops getStops() {
        Stops stops = new Stops();
        stops.add(new Stop(1));
        stops.add(new Stop(2));
        return stops;
    }

    private Stops getStop() {
        Stops stops = new Stops();
        stops.add(new Stop(STOP_ID));
        return stops;
    }

}
