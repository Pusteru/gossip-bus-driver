import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class WorkingDayShould {

    @Test
    public void know_when_drivers_never_met() {
        WorkingDay workingDay = new WorkingDay(480);
        workingDay.addDriver(new Driver(1, new Gossips(new Gossip(1)), getStops()));
        workingDay.addDriver(new Driver(2, new Gossips(new Gossip(2)), getStops2()));
        assertThat(workingDay.stopsTaken()).isEqualTo("never");
    }

    @Test
    public void know_many_steps_taken_to_share_all_gossips() {
        WorkingDay workingDay = new WorkingDay(480);
        workingDay.addDriver(new Driver(1, new Gossips(new Gossip(1)), getStops3()));
        workingDay.addDriver(new Driver(2, new Gossips(new Gossip(2)), getStops4()));
        assertThat(workingDay.stopsTaken()).isEqualTo("2");
    }

    private Stops getStops4() {
        Stops stops = new Stops();
        stops.add(new Stop(2));
        stops.add(new Stop(5));
        stops.add(new Stop(1));
        return stops;
    }

    private Stops getStops3() {
        Stops stops = new Stops();
        stops.add(new Stop(3));
        stops.add(new Stop(4));
        stops.add(new Stop(1));
        return stops;
    }

    private Stops getStops2() {
        Stops stops = new Stops();
        stops.add(new Stop(2));
        stops.add(new Stop(1));
        return stops;
    }

    private Stops getStops() {
        Stops stops = new Stops();
        stops.add(new Stop(1));
        stops.add(new Stop(2));
        return stops;
    }
}
