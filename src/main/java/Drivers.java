import java.util.ArrayList;
import java.util.List;

public class Drivers {

    List<Driver> drivers;

    public Drivers() {
        this.drivers = new ArrayList<>();
    }

    public void add(Driver driver) {
        drivers.add(driver);
    }

    public Gossips getGossipsFrom(int id) {
        return drivers.stream()
                .filter(d -> d.hasId(id)).findFirst()
                .get().getGossips();
    }

    public void shareGossips() {
        drivers.forEach(sharer -> drivers.stream()
                .filter(sharer::sameStopAs)
                .forEach(sharer::receiveGossipsFrom));
    }

    public boolean hasSpreadAllGossips() {
        return drivers.stream()
                .allMatch(driver -> driver.hasGossips(drivers.size()));
    }

    public void nextStops() {
        drivers.forEach(Driver::nextStop);
    }
}
