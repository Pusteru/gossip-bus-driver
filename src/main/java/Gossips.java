import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.*;

@EqualsAndHashCode
@ToString
public class Gossips {

    Set<Gossip> gossips;

    public Gossips() {
        this.gossips = new HashSet<>();
    }

    public Gossips(Gossip gossip) {
        this.gossips = new HashSet<>();
        this.gossips.add(gossip);
    }

    public void add(Gossip gossip) {
        gossips.add(gossip);
    }

    public void add(Gossips gossips) {
        this.gossips.addAll(gossips.getGossips());
    }

    private Set<Gossip> getGossips() {
        return gossips;
    }
}
