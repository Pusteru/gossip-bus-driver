import java.util.ArrayList;
import java.util.List;

public class Stops {

    List<Stop> stops;
    private int position;


    public Stops() {
        this.stops = new ArrayList<>();
        position = 0;
    }


    public void add(Stop stop) {
        stops.add(stop);
    }

    public Stop getCurrentStop() {
        return stops.get(position);
    }

    public boolean isInStop(Stop stop) {
        return stops.get(position).equals(stop);
    }

    public void nextStop() {
        position = (position +1) % stops.size();
    }
}
