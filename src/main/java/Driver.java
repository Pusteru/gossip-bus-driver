import lombok.EqualsAndHashCode;

@EqualsAndHashCode
public class Driver {
    private int id;
    private Gossips gossips;
    private Stops stops;

    public Driver(int id, Gossips gossips, Stops stops) {
        this.id = id;
        this.gossips = gossips;
        this.stops = stops;
    }

    public boolean hasId(int id) {
        return this.id == id;
    }

    public Gossips getGossips() {
        return gossips;
    }

    public boolean sameStopAs(Driver receiver) {
        return stops.isInStop(receiver.getCurrentStop());
    }

    private Stop getCurrentStop() {
        return stops.getCurrentStop();
    }

    public void receiveGossipsFrom(Driver receiver) {
        this.gossips.add(receiver.getGossips());
    }

    public boolean isInStop(Stop stop) {
        return stops.isInStop(stop);
    }

    public void nextStop() {
        stops.nextStop();
    }

    public boolean hasGossips(int number) {
        return gossips.gossips.size() == number;
    }
}
