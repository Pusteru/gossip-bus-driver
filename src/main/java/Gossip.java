import lombok.EqualsAndHashCode;

@EqualsAndHashCode
public class Gossip {
    private int id;

    public Gossip(int id) {

        this.id = id;
    }
}
