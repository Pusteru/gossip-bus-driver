import lombok.EqualsAndHashCode;

@EqualsAndHashCode
public class Stop {

    private int id;

    public Stop(int id) {
        this.id = id;
    }
}
