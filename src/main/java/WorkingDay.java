public class WorkingDay {
    private int minutes;
    private Drivers drivers;

    public WorkingDay(int minutes) {
        this.drivers = new Drivers();
        this.minutes = minutes;
    }

    public void addDriver(Driver driver) {
        drivers.add(driver);
    }

    public String stopsTaken() {
        for(int minute = 0; minute < minutes; minute++){
            drivers.shareGossips();
            if(drivers.hasSpreadAllGossips()){
                return String.valueOf(minute);
            }
            drivers.nextStops();
        }
        return "never";
    }
}
